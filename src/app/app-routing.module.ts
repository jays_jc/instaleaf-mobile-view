import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ThemeModule } from "./theme/theme.module";

const routes: Routes = [
  {
    path: "",
    // loadChildren: 'app/theme/theme.module#ThemeModule',
    loadChildren: () => ThemeModule,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
