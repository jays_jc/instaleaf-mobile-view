import { LoginComponent } from "./login/login.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { VerificationComponent } from "./verification/verification.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";

const routes: Routes = [
  {
    path: "login-signup",
    component: LoginComponent,
  },
  {
    path: "verify",
    component: VerificationComponent,
  },
  // {
  // 	path: 'terms',
  // 	component: RegisterTermsComponent
  // },{
  // 	path: 'privacy-policy',
  // 	component: PrivacyPolicyComponent
  // },
  {
    path: "forgot-password",
    component: ForgotPasswordComponent,
  },
  {
    path: "reset/:id",
    component: ResetPasswordComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
